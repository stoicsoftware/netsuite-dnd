# DND on NetSuite

A repository which uses the tabletop game _Dungeons & Dragons_ to teach system
design concepts for NetSuite and SuiteScript customizations.

Join the 
[Sustainable SuiteScript community](https://stoic.software/signup/)
for more SuiteScript guidance and advice.
